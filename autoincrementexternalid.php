<?php

require_once 'autoincrementexternalid.civix.php';

define('CUSTOM_EXTERNAL_ID_START_NUMBER', '14984');
/**
 * Implementation of hook_civicrm_config
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function autoincrementexternalid_civicrm_config(&$config) {
  _autoincrementexternalid_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function autoincrementexternalid_civicrm_xmlMenu(&$files) {
  _autoincrementexternalid_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function autoincrementexternalid_civicrm_install() {
  //To create custom field using XML
  $extensionDir = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;
  $customDataXMLFile = $extensionDir  . '/xml/CustomGroupData.xml';
  $import = new CRM_Utils_Migrate_Import( );
  $import->run( $customDataXMLFile );
  
  
  //Update the custom_external_id to existing individual contact
  $selectQuery = "SELECT id FROM civicrm_contact WHERE contact_type = 'Individual' ORDER BY id";
  $dao = CRM_Core_DAO::executeQuery($selectQuery);
  $count = CUSTOM_EXTERNAL_ID_START_NUMBER + 1;
  while ($dao->fetch()) {
    $values[] = "(".$dao->id .", ".$count++.")";
  }
  
  $insertQuery = "INSERT INTO civicrm_value_custom_external_id (entity_id, external_id) VALUES ".implode(', ', $values)." 
                  ON DUPLICATE KEY UPDATE entity_id = VALUES(entity_id), external_id = VALUES(external_id)";
  CRM_Core_DAO::executeQuery($insertQuery);
  require_once 'CRM/Autoincrementexternalid/Utils.php';
  CRM_Core_BAO_Setting::setItem($count,
    CRM_Autoincrementexternalid_Utils::CUSTOM_SETTING_GROUP,
    'civi_max_membership_no'
  );
  return _autoincrementexternalid_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function autoincrementexternalid_civicrm_uninstall() {
  return _autoincrementexternalid_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function autoincrementexternalid_civicrm_enable() {
  
  // to reset the civicrm autoincrement membership number setting
  $getMaxExternalId = CRM_Core_DAO::singleValueQuery('SELECT MAX(external_id) FROM civicrm_value_custom_external_id');
  $update_Number    = ($getMaxExternalId) ? $getMaxExternalId : CUSTOM_EXTERNAL_ID_START_NUMBER;
  
  //Update the custom_external_id to existing individual contact which doesn't have membership number
  $selectQuery = "SELECT cc.id 
    FROM civicrm_contact cc
    WHERE cc.contact_type = 'Individual' AND cc.id NOT IN ( SELECT cce.entity_id FROM civicrm_value_custom_external_id cce ) 
    ORDER BY cc.id";
  $dao = CRM_Core_DAO::executeQuery($selectQuery);
  $count = $update_Number + 1;
  while ($dao->fetch()) {
    $values[] = "(".$dao->id .", ".$count++.")";
  }
  
  $insertQuery = "INSERT INTO civicrm_value_custom_external_id (entity_id, external_id) VALUES ".implode(', ', $values)." 
                  ON DUPLICATE KEY UPDATE entity_id = VALUES(entity_id), external_id = VALUES(external_id)";
  CRM_Core_DAO::executeQuery($insertQuery);
  require_once 'CRM/Autoincrementexternalid/Utils.php';
  CRM_Core_BAO_Setting::setItem($count,
    CRM_Autoincrementexternalid_Utils::CUSTOM_SETTING_GROUP,
    'civi_max_membership_no'
  );
  
      
  return _autoincrementexternalid_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function autoincrementexternalid_civicrm_disable() {
  return _autoincrementexternalid_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function autoincrementexternalid_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _autoincrementexternalid_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function autoincrementexternalid_civicrm_managed(&$entities) {
  return _autoincrementexternalid_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_caseTypes
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function autoincrementexternalid_civicrm_caseTypes(&$caseTypes) {
  _autoincrementexternalid_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implementation of hook_civicrm_alterSettingsFolders
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function autoincrementexternalid_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _autoincrementexternalid_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implementation of hook_civicrm_post
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_post
 */
function autoincrementexternalid_civicrm_post ($op, $objectName, $id, &$objectRef) {
  if (!empty($objectRef->contact_type) && ($op == 'edit' || $op == 'create')){

    $external_identifier = CRM_Autoincrementexternalid_Utils::getExternalIdentifier($id);

    $cfId = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_CustomField', CRM_Autoincrementexternalid_Utils::CUSTOM_FIELD_NAME, 'id', 'name');

    if (!$external_identifier && $cfId) {
      
      //Don't know the latest external identifier and can't do max on as it is mix and match, that is why assigning random starting value 20000
      $maxMembershipNo = CRM_Core_BAO_Setting::getItem(CRM_Autoincrementexternalid_Utils::CUSTOM_SETTING_GROUP,
        'civi_max_membership_no', NULL, CUSTOM_EXTERNAL_ID_START_NUMBER
      );
      
      $params = array(
        'version' => 3,
        'entity_id' => $id,
        "custom_{$cfId}" => $maxMembershipNo
      );
      $result = civicrm_api3('CustomValue', 'create', $params);
    
      $maxMembershipNo = $maxMembershipNo + 1;
      CRM_Core_BAO_Setting::setItem($maxMembershipNo,
        CRM_Autoincrementexternalid_Utils::CUSTOM_SETTING_GROUP,
        'civi_max_membership_no'
      );
    }
  }
}
