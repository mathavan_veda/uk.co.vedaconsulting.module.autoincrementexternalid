<?php

class CRM_Autoincrementexternalid_Utils {
  
  const CUSTOM_SETTING_GROUP = 'Custom AutoIncrement Preferences';
  const CUSTOM_GROUP_NAME    = 'Custom_External_Id';
  const CUSTOM_FIELD_NAME    = 'External_Id';
    
  static function getExternalIdentifier($contactID) {
    if (!$contactID) {
      return FALSE;
    }
    $query = "
      SELECT external_id as external_id
      FROM civicrm_value_custom_external_id 
      WHERE  entity_id = %1 " ;

    $dao = CRM_Core_DAO::executeQuery($query, array(1 => array($contactID, 'Integer')));
    while ($dao->fetch()) {
      if((($dao->external_id != NULL) && ($dao->external_id > 0))) {
        $externalID = $dao->external_id;
        return $externalID;
      }
    }
    return NULL;
  }
}
